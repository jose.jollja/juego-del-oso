def validar_punto(tabla, fila, columna):
    puntaje = 0
    cantidadFilas = len(tabla)
    cantidadColumnas = len(tabla[0])

    if (columna >= 0 and columna < cantidadColumnas - 1 and tabla[fila][columna - 1] == 'O' and tabla[fila][columna + 1] == 'O'):
        puntaje += 1
    if (fila > 0 and fila < cantidadFilas - 1 and tabla[fila - 1][columna] == 'O' and tabla[fila + 1][columna] == 'O'):
        puntaje += 1
    if (fila > 0 and fila < cantidadFilas - 1 and columna > 0 and columna < cantidadColumnas - 1 and tabla[fila - 1][columna - 1] == 'O' and tabla[fila + 1][columna + 1] == 'O'):
        puntaje += 1
    if (fila > 0 and fila < cantidadFilas - 1 and columna > 0 and columna < cantidadColumnas - 1  and tabla[fila - 1][columna + 1] == 'O' and tabla[fila + 1][columna - 1] == 'O'):
        puntaje += 1

    if puntaje > 0:
        return True, puntaje
    else:
        return False, puntaje
