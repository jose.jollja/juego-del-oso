def dibujar_tabla(tabla):
    filas = len(tabla)
    columnas = len(tabla[0])

    print("   ", end="")
    for i in range(columnas):
        print(f" {i}  ", end="")
    print()

    print("  +" + "---+" * columnas)

    for i in range(filas):
        print(f"{i} |", end="")
        for j in range(columnas):
            valor = tabla[i][j]
            if valor is None:
                valor = " "
            print(f" {valor} |", end="")
        print()
        print("  +" + "---+" * columnas)