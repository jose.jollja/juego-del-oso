#Grupo 7
#Integrantes
#Alcazar Alvarez, Carlos Manuel        202310490
#Chambi Mamani Emiliano Efraín         202310178
#Carrizales Ventocilla, Carlos Luis    202310526
#Jollja Minaya José Luis               202310726
#Matias Celestino, Jeremy Samuel       202120672

from ganador import ganador
from matriz_inicial import matriz_inicial
from ingresar_valor import ingresar_valor
from dibujar_tabla import dibujar_tabla
from ingresar_musica import ingresar_musica
import threading

def main():
    # Iniciar un hilo para reproducir la música en segundo plano
    hilo_musica = threading.Thread(target=ingresar_musica)
    hilo_musica.start()
    # Crear una tabla de 8x8 inicialmente con valores nulos
    valorFila=8
    valorColumna=8
    tabla = matriz_inicial(valorFila,valorColumna)

    # Mostrar la matriz inicial antes de ingresar los datos
    dibujar_tabla(tabla)
    print()

    # Variables para llevar el puntaje de cada jugador
    puntaje_jugador1 = 0
    puntaje_jugador2 = 0

    # Bucle para ingresar valores hasta que no haya más celdas vacías
    jugador_vuelve_a_jugar = False
    # Jugador inicial
    jugador = 1


    while any(None in fila for fila in tabla):
        puntaje = 0
        if jugador == 1:
            tabla, valor, puntaje = ingresar_valor(tabla, jugador)
            dibujar_tabla(tabla)
            if valor == 'S':
                puntaje_jugador1 += puntaje
                if puntaje == 1:
                    print("¡El jugador 1 ha ganado 1 punto!")
                else:
                    print("¡El jugador 1 ha ganado ", puntaje, " puntos!")
                jugador_vuelve_a_jugar = True
        elif jugador == 2:
            tabla, valor, puntaje = ingresar_valor(tabla, jugador)
            dibujar_tabla(tabla)
            if valor == 'S':
                puntaje_jugador2 += puntaje
                if puntaje == 1:
                    print("¡El jugador 2 ha ganado 1 punto!")
                else:
                    print("¡El jugador 2 ha ganado ", puntaje, " puntos!")
                jugador_vuelve_a_jugar = True
        print(f"Puntaje: Jugador 1: {puntaje_jugador1}, Jugador 2: {puntaje_jugador2}\n")

        if jugador_vuelve_a_jugar:
            print(f"¡El jugador {jugador} vuelve a jugar!")
            jugador_vuelve_a_jugar = False
        else:
            jugador = 2 if jugador == 1 else 1

    ganador(puntaje_jugador1, puntaje_jugador2)

main()