from validar_punto import validar_punto
def ingresar_valor(tabla, jugador):
    cantidadFilas = len(tabla)
    cantidadColumnas = len(tabla[0])
    while True:
        try:
            fila = int(input(f"Jugador {jugador}, ingrese la fila (0-7): "))
            columna = int(input(f"Jugador {jugador}, ingrese la columna (0-7): "))
            validacion= False
            puntaje = 0
            if fila < 0 or fila > cantidadFilas-1 or columna < 0 or columna > cantidadColumnas-1:
                print("Posición inválida. Intente nuevamente.")
                continue

            if tabla[fila][columna] is not None:
                print("Esta posición ya contiene un valor. Intente nuevamente.")
                continue

            while True:
                valor = input(f"Jugador {jugador}, ingrese la letra (O/S): ")
                if valor == 'O' or valor == 'S':
                    break
                else:
                    print("Valor inválido. Solo se permite ingresar 'O' o 'S'. Intente nuevamente.")

            if valor == 'S':
                validacion, puntaje=validar_punto(tabla, fila, columna)
                if not validacion:
                    print("No se cumple la condición para colocar una 'S'. Intente nuevamente.")
                    continue

            tabla[fila][columna] = valor
            break
        except ValueError:
            print("Entrada inválida. Ingrese un número entero para la fila y la columna.")

    return tabla, valor, puntaje