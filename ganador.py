def ganador(puntaje_jugador1, puntaje_jugador2):
    color_amarillo = "\033[93m"
    color_fondo_negro = "\033[40m"
    color_reset = "\033[0m"
    ganador = 0
    if puntaje_jugador1 > puntaje_jugador2:
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "WW        WW      II      NN    NN                PPPPPP    LL              AA        YY   YY    EEEEEEE     RRRRRRR                1111" " "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "WW        WW      II      NNN   NN                PP   P    LL             A  A        YY YY     EE          RR    RR              11 11" " "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "WW    W   WW      II      NN N  NN                PP   P    LL            A    A        YYY      EEEE        RR   RR              11  11" " "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "WW   WWW  WW      II      NN  N NN                PPPPP     LL           AAAAAAAA        Y       EEEE        RRRRR                    11" " "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " " WW   W  WW       II      NN   NNN                PP        LL          AA      AA       Y       EE          RR   RR                  11" " "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "  WW    WW        II      NN    NN                PP        LLLLLLL    AA        AA      Y       EEEEEEE     RR    RR              11111111" " "" "" "" "" "" " + color_reset)

    elif puntaje_jugador2 > puntaje_jugador1:
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "WW        WW      II      NN    NN                PPPPPP    LL              AA        YY   YY    EEEEEEE     RRRRRRR                22222" " "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "WW        WW      II      NNN   NN                PP   P    LL             A  A        YY YY     EE          RR    RR              2     2" " "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "WW    W   WW      II      NN N  NN                PP   P    LL            A    A        YYY      EEEE        RR   RR                   2" " "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "WW   WWW  WW      II      NN  N NN                PPPPP     LL           AAAAAAAA        Y       EEEE        RRRRR                   2" " "" "" "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " " WW   W  WW       II      NN   NNN                PP        LL          AA      AA       Y       EE          RR   RR                2" " "" "" "" "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" " "  WW    WW        II      NN    NN                PP        LLLLLLL    AA        AA      Y       EEEEEEE     RR    RR              2222222" " "" "" "" "" "" "" " + color_reset)

    else:
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" ""EEEEEEE     MM    MM     PPPPP        AAAAA      TTTTTTT     EEEEEEE" " "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" ""EE          MMM  MMM     PP   PP     AA   AA       TT        EE" " "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" ""EEEEE       MM MM MM     PPPPP       AAAAAAA       TT        EEEEE" " "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" ""EE          MM    MM     PP          AA   AA       TT        EE" " "" "" "" "" "" "" "" "" " + color_reset)
        print(
            color_fondo_negro + color_amarillo + " "" "" "" "" "" "" "" "" ""EEEEEEE     MM    MM     PP          AA   AA       TT        EEEEEEE" " "" "" "" " + color_reset)
